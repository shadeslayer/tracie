#!/usr/bin/env bash
set -x

for trace_images in $(git lfs ls-files -n -I \*.png)
do
    git lfs pull -I $trace_images
done