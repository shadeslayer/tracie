#!/usr/bin/env python3
import os
import re

CI_PROJECT_DIR = os.environ['CI_PROJECT_DIR']
DEVICE_NAME = os.environ['DEVICE_NAME']

with os.popen("git lfs ls-files -n -I \*.trace") as pipe:
    for trace in pipe:
        _trace = re.sub(r"[;,]", r"\?", trace.strip())
        os.system(f"git lfs pull -I {_trace}")
        os.system(f"python3 {CI_PROJECT_DIR}/scripts/dump_trace_images.py --trace-types apitrace --device-name {DEVICE_NAME} {_trace}")
        os.system(f"python3 {CI_PROJECT_DIR}/scripts/diff_trace_images.py --device-name {DEVICE_NAME} --output-dir {DEVICE_NAME}/results {_trace}")
        os.remove(trace.strip())
